from io import BytesIO
from pathlib import Path
import os.path
import shutil
from datetime import date
from typing import Optional, NoReturn
from pdfrw import PdfReader, PdfWriter, PageMerge
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4

FILEDIR = os.path.dirname(__file__)

class Person:
    def __init__(self, first_name=None, family_name=None, number=None):
        self.first_name = first_name
        self.family_name = family_name
        # SYBOS Bekleidungsnummer
        self.number = number

    def __str__(self):
        return f"{self.first_name} {self.family_name} ({self.number})"

class Einsatzkleidung:
    # overwritten by instance attributes
    owner: Optional[Person] = None
    type  = None

    def query_ids(self) -> NoReturn:
        raise NotImplementedError

    def print_pdf(self, filename: Path):
        raise NotImplementedError

    def __str__(self):
        return f"{self.type} von {self.owner}"

class Einsatzjacke(Einsatzkleidung):
    type: str = "Einsatzjacke"
    pdfform = Path(os.path.join(FILEDIR, "Schutzjacke.pdf"))

    def __init__(self, owner=None):
        self.owner = owner
        self._id_jacke = None
        self._id_schlaufe = None
        self._id_karabiner = None

    def query_ids(self) -> NoReturn:
        self.id_jacke = input("Jackennummer: ")
        self.id_schlaufe = input("Schlaufennummer: ")
        self.id_karabiner = input("Karabinernummer: ")

    @property
    def id_jacke(self) -> Optional[int]:
        return self._id_jacke

    @id_jacke.setter
    def id_jacke(self, value: Optional[int]):
        """set the Jacken id
        """
        self._id_jacke = value

    @property
    def id_schlaufe(self):
        return self._id_schlaufe

    @id_schlaufe.setter
    def id_schlaufe(self, value):
        """set the Schlaufen id
        """
        self._id_schlaufe = value

    @property
    def id_karabiner(self):
        return self._id_karabiner

    @id_karabiner.setter
    def id_karabiner(self, value: Optional[int]):
        """set the Karabiner id
        """
        self._id_karabiner = value

    def save_pdf(self, filename: Path):
        #shutil.copyfile(self.pdfform, filename)
        ipdf = PdfReader(self.pdfform)

        packet = BytesIO()
        # create a new PDF with Reportlab
        can = canvas.Canvas(packet, pagesize=A4)
        if self.owner is not None:

            can.drawString(483,610, f"{self.owner.number//10000}")
            can.drawString(499,610, f"{self.owner.number//1000%10}")
            can.drawString(516,610, f"{self.owner.number//100%10}")
            can.drawString(533,610, f"{self.owner.number//10%10}")
            can.drawString(550,610, f"{self.owner.number%10}")
            can.drawString(110,607, f"{self.owner.family_name}, {self.owner.first_name}")
        if self.id_jacke is not None:
            can.drawString(107,550, str(self.id_jacke))
        if self.id_schlaufe is not None:
            can.drawString(302,550, str(self.id_schlaufe))
        if self.id_karabiner is not None:
            can.drawString(488,550, str(self.id_karabiner))

        # fill in date
        can.drawString(483,579, date.today().strftime("%d.%m.%Y"))
        can.save()
        # move to the beginning of the buffer
        packet.seek(0)
        form_page = PdfReader(packet).pages[0]
        for page in ipdf.pages:
            PageMerge(page).add(form_page).render()
        PdfWriter(filename).addpages(ipdf.pages).write()

    def __str__(self):
        return super().__str__() + f" mit Nr. {self.id_jacke}, Schlaufe {self.id_schlaufe} und Karabiner {self.id_karabiner}"


class Einsatzhose(Einsatzkleidung):
    type = "Einsatzhose"
    pdfform = Path(os.path.join(FILEDIR, "Schutzhose.pdf"))

    def __init__(self, owner=None):
        self.owner = owner
        self._id_hose = None
        self._id_hosentraeger = None

    def query_ids(self) -> NoReturn:
        self.id_hose = input("Hosennummer: ")
        self.id_hosentraeger = input("Hosenträgernummer: ")

    @property
    def id_hose(self):
        return self._id_hose

    @id_hose.setter
    def id_hose(self, value):
        self._id_hose = value

    @property
    def id_hosentraeger(self):
        return self._id_hosentraeger

    @id_hosentraeger.setter
    def id_hosentraeger(self, value):
        self._id_hosentraeger = value

    def __str__(self):
        return super().__str__() + f" mit Nr. {self.id_hose} und Hosenträger Nr. {self.id_hosentraeger}"

    def save_pdf(self, filename: Path):
        #shutil.copyfile(self.pdfform, filename)
        ipdf = PdfReader(self.pdfform)

        packet = BytesIO()
        # create a new PDF with Reportlab
        can = canvas.Canvas(packet, pagesize=A4)
        if self.owner is not None:

            can.drawString(483,602, f"{self.owner.number//10000}")
            can.drawString(499,602, f"{self.owner.number//1000%10}")
            can.drawString(516,602, f"{self.owner.number//100%10}")
            can.drawString(533,602, f"{self.owner.number//10%10}")
            can.drawString(550,602, f"{self.owner.number%10}")
            can.drawString(116,601, f"{self.owner.family_name}, {self.owner.first_name}")

        if self.id_hose is not None:
            can.drawString(115,538, str(self.id_hose))
        if self.id_hosentraeger is not None:
            can.drawString(398,538, str(self.id_hosentraeger))

        # fill in date
        can.drawString(483,570, date.today().strftime("%d.%m.%Y"))
        can.save()
        # move to the beginning of the buffer
        packet.seek(0)
        form_page = PdfReader(packet).pages[0]
        for page in ipdf.pages:
            PageMerge(page).add(form_page).render()
        PdfWriter(filename).addpages(ipdf.pages).write()

# Allow to run python3 main.py interactively
if __name__ == '__main__':
    Personal = (
        Person("Beast", "The", 666),
    )

    number = int(input("SYBOS Kleidernummer (hint use 666): "))
    persons = list(filter(lambda x: x.number==number, Personal))
    if len(persons):
        owner = persons[0]
        print (f"Hallo {owner}")
    else:
        print ("Unbekannte Nummer, bitte vollen Daten eingeben")
        first_name = input("Vorname: ")
        family_name = input("Nachname: ")
        owner = Person(first_name=first_name,
                       family_name=family_name,
                       number=number)

    print("Hose (1) oder Jacke (2) reinigen?")
    type = int(input("> "))
    if type==1:
        piece = Einsatzhose(owner=owner)
    elif type==2:
        piece = Einsatzjacke(owner=owner)
    else:
        print("Ich nix verstehen, ich kaputt")

    piece.query_ids()
    print(piece)
    piece.save_pdf('tmp.pdf')
