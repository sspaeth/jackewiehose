import sqlite3
from . import Person
from pathlib import Path
from flask import current_app

def Personal_from_db(dbfile: Path):
    """Load and return the list of Person objects"""
    Personal = []
    db = sqlite3.connect(
        dbfile,
        detect_types=sqlite3.PARSE_DECLTYPES
    )
    #db.row_factory = sqlite3.Row
    cur = db.cursor()

    for row in cur.execute('SELECT first_name, family_name, number FROM Personal'):
        Personal.append(Person(row[0], row[1], row[2]))
    return Personal
