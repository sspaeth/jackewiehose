"""WSGI config for JackeWieHose.

It exposes the WSGI callable as a module-level variable named ``application``.
"""

import os, sys

# Add this directory and ./venv (if existing) to the python path
sys.path.append(os.path.dirname(__file__))

venvpath=os.path.join(os.path.dirname(__file__), 'venv/lib/python3.7/site-packages')
if os.path.isdir(venvpath):
    sys.path.append(venvpath)

from JackeWieHose import app as application
