#!/usr/bin/env python3
from flask import Flask, render_template, redirect, request, session, url_for, flash
import logging, os, sys, uuid
from pathlib import Path

from jackewiehose import Einsatzjacke, Einsatzhose, Personal_from_db

class DefaultConfig(object):
   PDFOUTDIR = 'tmp'
   DATABASE = 'personal.db'

NAME="JackeWieHose"
app = Flask(NAME)
app.config.from_object(DefaultConfig)
if not app.config.from_pyfile("config_local.py"):
    logging.info("Did not find config_local.py local configuration")

if not app.secret_key:
    logging.fatal("No app.SECRET_KEY configured. Check your configuration")
    sys.exit(1)


if app.debug:
    from werkzeug.debug import DebuggedApplication
    app.wsgi_app = DebuggedApplication(app.wsgi_app, True)

# load a list of Person objects to use
Personal = Personal_from_db(app.config['DATABASE'])

@app.route("/")
def index():
    if 'sybos_num' in session:
            return render_template("index.html", sybos_num=session["sybos_num"])
    return redirect(url_for('login'))


@app.route("/jacke", methods=['GET', 'POST'])
def enter_jacke():
    error = None
    if (request.method == 'POST'):
        persons = list(filter(
            lambda x: x.number==int(session.get('sybos_num')), Personal))
        if len(persons):
            # The person exists
            owner = persons[0]
            jacke = Einsatzjacke(owner=owner)
            try:
                jacke.id_jacke = request.form['id_jacke']
            except ValueError:
                jacke.id_jacke = None
                error = "Jackennr war keine Nummer"
                flash("Jackennr war keine Nummer2")
            try:
                jacke.id_schlaufe = request.form['id_schlaufe']
            except ValueError:
                jacke_id_schlaufe = None
                error = "Schlaufennr war keine Nummer"
                flash("Schlaufennr war keine Nummer2")
            try:
                jacke.id_karabiner = request.form['id_karabiner']
            except ValueError:
                jacke.id_karabiner = None
                error = "Karabinernr war keine Nummer"
                flash("Karabinernr war keine Nummer2")

            id = uuid.uuid4()
            pdfpath = os.path.join(app.config["PDFOUTDIR"], f"{id}.pdf")
            jacke.save_pdf(Path(pdfpath))
            return redirect(url_for("static", filename=f"forms/{id}.pdf"))
        else:
            error = "Ungültige Kleidernummer"
            flash("Ungültige Kleidernummer2")
    # GET
    return render_template("jacke.html", error=error)

@app.route("/hose", methods=['GET', 'POST'])
def enter_hose():
    error = None
    if request.method == 'POST':
        persons = list(filter(
            lambda x: x.number==int(session.get('sybos_num')), Personal))
        if len(persons):
            # The person exists
            owner = persons[0]
            hose = Einsatzhose(owner=owner)
            try:
                hose.id_hose = request.form['id_hose']
            except ValueError:
                jacke.id_hose = None
                error = "Hosennr war keine Nummer"
                flash("Hosennr war keine Nummer2")
            try:
                hose.id_hosentraeger = request.form['id_hosentraeger']
            except ValueError:
                jacke.id_karabiner = None
                error = "Karabinernr war keine Nummer"
                flash("Karabinernr war keine Nummer2")
            id = uuid.uuid4()
            pdfpath = os.path.join(app.config["PDFOUTDIR"], f"{id}.pdf")
            hose.save_pdf(Path(pdfpath))
            return redirect(url_for("static", filename=f"forms/{id}.pdf"))
        else:
            error = "Ungültige Kleidernummer"
            flash("Ungültige Kleidernummer2")
    # GET
    return render_template("hose.html", error=error)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        persons = list(filter(
            lambda x: x.number==int(request.form['sybos_num']), Personal))
        print(persons, int(request.form['sybos_num']))
        if len(persons):
            # The person exists
            owner = persons[0]
            session['sybos_num'] = request.form['sybos_num']
        else:
            flash('Unbekannte Kleidernummer')
        return redirect(url_for('index'))
    return render_template("login.html")

@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('sybos_num', None)
    return redirect(url_for('index'))
