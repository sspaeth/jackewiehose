# Copy this file to config_local.py in order to make use of it

# Set the secret key to some random bytes. Keep this really secret!
SECRET_KEY = b'sdm,dfsgldfjkdfjkfdjk'
# Relative or absolute directory to save filled in PDF forms to.
PDFOUTDIR = 'tmp'
# Check the README for how to create the personal db
DATABASE = 'personal.db'

# 'production' or 'development'
ENV = 'production'
# DEBUG = False
# TESTING = False
#TEMPLATES_AUTO_RELOAD = False

PREFERRED_URL_SCHEME="https"
