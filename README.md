# jackewiehose

Manage clothing forms for the Feuerwehr :-)

## Getting started

This is a flask web application. It is recommended to create a venv environment in the "venv" directory which contains the missing dependencies if they are not system wide available.

## Dependencies

- flask (pip install flask)
- pdfrw (pip install pdfrw)
- reportlab (pip install reportlab)

## Restarting

If run under passenger phusion, the application can be restarted by `touch`ing tmp/restart.txt.

## Configuration

In the top-level dir, cp file config.py to config_local.py to change
configuration values.

User data can be stored in a sqlite database

sqlite3 personal.db

CREATE TABLE Personal (
  number INTEGER PRIMARY KEY,
  first_name TEXT NOT NULL,
  family_name TEXT NOT NULL
);

And add new persons as
INSERT into Personal (number, first_name, family_name) VALUES(123, 'Sebastian', 'Späth');

## License

jackewiehose is licensed under the GNU AFFERO GENERAL PUBLIC LICENSE
version 3, or at your option, or any later version. (GNU APGL 3+)